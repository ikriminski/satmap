<?php
#this is a representation of a rotation matrix.
#Rotates <x,y,z> about <a,b,c> around axis <u,v,w> by theta. <u,v,w> must be unit vector.
function rotation_matrix($x,$y,$z,$a,$b,$c,$u,$v,$w,$theta)
{
		$xout = ($a*($v ** 2 + $w ** 2) - $u*($b*$v + $c*$w-$u*$x-$v*$y-$w*$z))*(1-cos($theta))+$x*cos($theta)+(-$c*$v+$b*$w-$w*$y+$v*$z)*(sin($theta));
		$yout = ($b*($u ** 2 + $w ** 2) - $v*($a*$u + $c*$w-$u*$x-$v*$y-$w*$z))*(1-cos($theta))+$y*cos($theta)+($c*$u-$a*$w+$w*$x-$u*$z)*(sin($theta));	
		$zout = ($c*($u ** 2 + $v ** 2) - $w*($a*$u + $b*$v-$u*$x-$v*$y-$w*$z))*(1-cos($theta))+$z*cos($theta)+(-$b*$u+$a*$v-$v*$x+$u*$y)*(sin($theta));
		return array($xout, $yout, $zout);
}
function scale($vector){
	$magnitude = ($vector[0]**2 + $vector[1]**2 + $vector[2]**2)**(1/2);
	return array($vector[0]/$magnitude,$vector[1]/$magnitude,$vector[2]/$magnitude);
}
function magnitude($vector){
	$magnitude = ($vector[0]**2 + $vector[1]**2 + $vector[2]**2)**(1/2);
	return $magnitude;
}
function ellipse_phi($theta, $major_axis, $minor_axis){
	$phi = atan(($minor_axis/$major_axis)* tan($theta));
	
	return $phi;
}
function ellipse_theta($phi, $major_axis, $minor_axis){
	$theta = atan2($major_axis*sin($phi),$minor_axis*cos($phi));
	if($theta < 0){
		$theta = 2*M_PI + $theta;
	}
	return $theta;
}
function search_file($name){
	$handle = fopen("tle.data", "r");
	if ($handle) {
		while (($buffer = fgets($handle)) !== false) {
			$buffer = preg_replace('/\s+/', '', $buffer);
			if(strcmp($buffer, $name)==0){			
				return array(fgets($handle),fgets($handle));
				break;
			}
		}
		fclose($handle);
	}
		
}


echo json_encode(get_orbit_data($_GET["name"]));

function get_orbit_data($name){
	#read data
	$datalines = search_file(preg_replace('/\s+/', '', $name));
	$line1= $datalines[0];
	$line2= $datalines[1];
	#print($line1."\n");
	$data1 = explode(" ", $line1);
	$data2 = explode(" ", $line2);
	#calculate parameters
	$inclination = deg2rad(floatval($data2[3]));
	$RA = deg2rad(floatval($data2[4]));
	$ecc = floatval("0." . $data2[5]);
	$perigee = deg2rad(floatval($data2[6]));
	$motion = floatval($data2[8]);
	$period = 86400/$motion;
	$anomaly = deg2rad($data2[7]);
	$major_axis = (($period ** 2)*(1.00925 * 10 ** 13))**(1/3);
	$minor_axis = $major_axis* (((1-$ecc ** 2))**(1/2));
	$focus = ($major_axis * $ecc);
	$earth = array($focus, 0 , 0);
	
	
	$orbit_positions = array();
	#calculate orbit
	for ($i = 0; $i < 2*M_PI*20; $i = $i + 1){
		$perp = array(0,0,1);
		$x = array($major_axis*cos($i/20), $minor_axis*sin($i/20), 0);
		
		$x = rotation_matrix($x[0],$x[1],$x[2],$earth[0],$earth[1],$earth[2],0,1,0,$inclination);
		$perp = rotation_matrix($perp[0],$perp[1],$perp[2],$earth[0],$earth[1],$earth[2],0,1,0,$inclination);
		$x = rotation_matrix($x[0],$x[1],$x[2],$earth[0],$earth[1],$earth[2],0,0,1,$RA);
		$perp = rotation_matrix($perp[0],$perp[1],$perp[2],$earth[0],$earth[1],$earth[2],0,0,1,$RA);
		$perp = scale($perp);
		$x = rotation_matrix($x[0],$x[1],$x[2],$earth[0],$earth[1],$earth[2],$perp[0],$perp[1],$perp[2],$RA-(M_PI/2));
		$x[0] = $x[0]-$earth[0];
		$orbit_positions[$i] = $x;
	}
	
	$calculated_anomaly = array();
	$phi = 0;
	$counter = 0;
	$theta = 0;
	$time = 0;
	
	#calculate positions in orbit for theta=0 -> theta=2pi
	while ($phi < 2*M_PI){
		$perp = array(0,0,1);
		$x = array($major_axis*cos(ellipse_theta($phi,$major_axis,$minor_axis)), $minor_axis*sin(ellipse_theta($phi,$major_axis,$minor_axis)), 0);
		
		$x = rotation_matrix($x[0],$x[1],$x[2],$earth[0],$earth[1],$earth[2],0,1,0,$inclination);
		$perp = rotation_matrix($perp[0],$perp[1],$perp[2],$earth[0],$earth[1],$earth[2],0,1,0,$inclination);
		$x = rotation_matrix($x[0],$x[1],$x[2],$earth[0],$earth[1],$earth[2],0,0,1,$RA);
		$perp = rotation_matrix($perp[0],$perp[1],$perp[2],$earth[0],$earth[1],$earth[2],0,0,1,$RA);
		$perp = scale($perp);
		$x = rotation_matrix($x[0],$x[1],$x[2],$earth[0],$earth[1],$earth[2],$perp[0],$perp[1],$perp[2],$RA-(M_PI/2));
		$x[0] = $x[0]-$earth[0];
		
		$phi = ($phi + ((($major_axis * $minor_axis) / (magnitude($x) ** 2))*0.001));	
		$calculated_anomaly[$counter] = $x;
		$counter = $counter + 1;
	}
	
	$out = array();
	$out[0] = $orbit_positions;
	$out[1] = $calculated_anomaly;
	$out[2] = $motion;
	$out[3] = $anomaly;
	return $out;
}

?>



